from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivymd.app import MDApp
from kivy.core.window import Window
from kivymd.toast import toast
from kivymd.uix.filemanager import MDFileManager
from youdownloader import Youdownlod


class HomeScreen(BoxLayout):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

        Window.size=(1200,750)
        self.youd = Youdownlod()
        self.pathvideo=None
        self.resolution=None
        self.list_btn_res=[]
        self.spacing=20
        self.orientation="vertical"
        self.title_notice()
        self.input_search()
        self.layout10()
        self.fm = MDFileManager(
            exit_manager=self.exit_manager,
            select_path=self.select_path,
            preview=False,)
        self.path_part()
        self.add_widget(self.btn)
        ###########################################
    def title_notice(self):
        self.label = Label(text="Youtube Downloader", bold=True, font_size=30, color="red", size_hint=(1, .12))
        self.add_widget(self.label)
        self.label1 = Label(text="To download your video : paste your link then choose the resolution you want, "
                                 "set your path and click download",font_size=18, color="red", size_hint=(1, .12))
        self.add_widget(self.label1)
    def input_search(self):
        self.grid_search=GridLayout(cols=2, rows=1,size_hint=(.9,.3),padding=30,spacing=20,pos_hint={"center_x":.5})
        self.link_video_input = TextInput(hint_text="Copy and paste your YouTube URL here: ",font_size=18)
        self.grid_search.add_widget(self.link_video_input)
        self.search_btn=Button(text="search",font_size=22,size_hint=(.2,.8))
        self.search_btn.bind(on_press=self.detail_video)

        self.grid_search.add_widget(self.search_btn)
        self.add_widget(self.grid_search)

    def layout10(self):

        self.btn = Button(text="Download",background_color="green",font_size=25, size_hint=(.2, .15), pos_hint={"center_x": .5},disabled=True)
        self.btn.bind(on_press=self.start_download)
        self.gridprincipale = GridLayout(cols=2, rows=1)
        self.grid_detail = BoxLayout(orientation="vertical",padding=30,spacing=10)
        self.gridprincipale.add_widget(self.grid_detail)
        self.gridresolution=BoxLayout(orientation="vertical",padding=30,spacing=10)
        self.gridprincipale.add_widget(self.gridresolution)
        self.add_widget(self.gridprincipale)
    def detail_video(self,*args):
        self.grid_detail.clear_widgets()
        self.gridresolution.clear_widgets()
        try:

            self.youd.detail(self.link_video_input.text)
            self.title_label=Label(text="video :  "+self.youd.title,font_size=20,halign="left",color="white")
            self.title_label.bind(size=self.title_label.setter('text_size'))
            self.grid_detail.add_widget(self.title_label)

            self.author_label = Label(text="Author :  "+self.youd.author, font_size=20,halign="left",color="white")
            self.author_label.bind(size=self.author_label.setter('text_size'))
            self.grid_detail.add_widget(self.author_label)

            self.length_label = Label(text="Length  : "+self.youd.length, font_size=20,halign="left",color="white")
            self.length_label.bind(size=self.length_label.setter('text_size'))
            self.grid_detail.add_widget(self.length_label)

            self.pubDate_label = Label(text="Publication date : "+self.youd.pubDate[:10], font_size=20,halign="left",color="white")
            self.pubDate_label.bind(size=self.pubDate_label.setter('text_size'))
            self.grid_detail.add_widget(self.pubDate_label)

            self.view_label = Label(text="Views  : "+self.youd.view, font_size=20,halign="left",color="white")
            self.view_label.bind(size=self.view_label.setter('text_size'))
            self.grid_detail.add_widget(self.view_label)
            self.buttonResolution()
        except:
            toast("ERROR. Check your link or your connection and Try again")
            self.grid_detail.clear_widgets()
            self.gridresolution.clear_widgets()

    def path_part(self):
        self.pathbox = GridLayout(cols=2, rows=1,size_hint=(.8,.3),padding=30,spacing=20,pos_hint={"center_x":.5})
        self.labelpath = Label(text="Set your path by clicking on 'set path button'",color="grey", font_size=20, size_hint=(.7, .9), halign="left",valign="middle")
        self.labelpath.bind(size=self.labelpath.setter('text_size'))
        self.pathbox.add_widget(self.labelpath)
        self.btnpath = Button(text="set path", font_size=20, size_hint=(.1, .9))
        self.btnpath.bind(on_press=self.open_file_manager)
        self.pathbox.add_widget(self.btnpath)
        self.add_widget(self.pathbox)

    def buttonResolution(self):
        self.youd.getresolution(self.link_video_input.text)
        for i,j in enumerate(self.youd.res):
            self.list_btn_res.append(Button(text=j))
            self.gridresolution.add_widget(self.list_btn_res[i])
            self.list_btn_res[i].bind(on_press=self.setResolution)
        self.btn_audio=Button(text='Audio only(mp3)')
        self.gridresolution.add_widget(self.btn_audio)
        self.btn_audio.bind(on_press=self.setAudio)

    def setResolution(self,widget):
        self.resolution=widget.text
        self.btn.disabled=False
        toast(self.resolution)
    def setAudio(self,*args):
        self.resolution="audio"
        self.btn.disabled=False
        toast("Audio (mp3)")

    def select_path(self,path):
        self.pathvideo=path
        self.labelpath.text=path
        self.exit_manager()
        toast(path)
    def open_file_manager(self,*args):
        self.fm.show("C:\\")

    def exit_manager(self):
        self.fm.close()
    def start_download(self,*args):
        self.youd.downloadY(self.resolution,self.pathvideo)

class YoutubeDownloader(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            home=HomeScreen()
            return home

YoutubeDownloader().run()



