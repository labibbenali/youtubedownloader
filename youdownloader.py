import os.path
from kivymd.toast import toast
from pytube import YouTube

class Youdownlod():
    def __init__(self):
        self.yt=None
        self.res_model=["144p","240p","360p","480p","720p"]
        self.res=[]

    def detail(self,lien):
        self.yt=YouTube(lien)
        self.title=self.yt.title
        self.author=self.yt.author
        self.length=self.set_length_video(self.yt.length)#/60),2))
        self.pubDate=str(self.yt.publish_date)
        self.view=str(self.yt.views)

    def downloadY(self,res,path=None,name=None):
        toast("Done")
        if res=="audio":
            self.audio = self.yt.streams.filter(only_audio=True).first().download(path)
            base, ext = os.path.splitext(self.audio)
            new_file = base + '.mp3'
            os.rename(self.audio, new_file)
        else:
            self.download=self.yt.streams.filter(file_extension="mp4",resolution=res).first().download(path,name)

    def getresolution(self,lien):
        self.yt=YouTube(lien)
        self.strm=self.yt.streams
        for i in self.strm:
            if i.resolution  in self.res_model and i.resolution not in self.res:
                self.res.append(i.resolution)
        self.res.sort(reverse=True)

    def set_length_video(self,vid_length):
        h=vid_length//3600
        m=(vid_length%3600)//60
        s=((vid_length%3600)%60)
        if h!=0:
            if s<10:
                return str(h)+":"+str(m)+":0"+str(s)
            else:
                return str(h)+":"+str(m)+":"+str(s)
        else:
            if s<10:
                return str(m)+":0"+str(s)
            else:
                return str(m)+":"+str(s)



